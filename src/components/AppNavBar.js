// React Bootstrap Components
/*
  - Syntax
    - import moduleName from 'filePath';
*/
import { useContext } from 'react';
import { Container, Navbar, Nav } from 'react-bootstrap';
import { Link, NavLink, useNavigate } from 'react-router-dom';
import UserContext from '../userContext';

export default function AppNavBar() {
  const navigate = useNavigate();
  const { user, setUser } = useContext(UserContext);

  const handleLogout = () => {
    setUser(null);
    navigate('/home');
  };

  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand as={Link} to="/home">
          Budol Budol
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={NavLink} to="/home">
              Home
            </Nav.Link>
            <Nav.Link as={NavLink} to="/product/active">
              Products
            </Nav.Link>
            {user && user.id !== undefined ? (
              <>
                <Nav.Link onClick={handleLogout} as={NavLink} to="/logout">
                  Logout
                </Nav.Link>
                {user.isAdmin && (
                  <Nav.Link as={NavLink} to="/profile/admin">
                    Admin
                  </Nav.Link>
                )}
                {!user.isAdmin && (
                  <Nav.Link as={NavLink} to="/profile">
                    Profile
                  </Nav.Link>
                )}
              </>
            ) : (
              <>
                <Nav.Link as={NavLink} to="/login">
                  Login
                </Nav.Link>
                <Nav.Link as={NavLink} to="/register">
                  Register
                </Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
