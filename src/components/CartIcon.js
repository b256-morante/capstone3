import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';

const CartIcon = ({ itemCount, onClick }) => {
	const handleClick = () => {
		console.log('Cart icon clicked'); // Verify the click event
		onClick();
	};

	return (
		<div className="cart-icon" onClick={handleClick}>
			<FontAwesomeIcon icon={faShoppingCart} />
			<span className="cart-item-count">{itemCount}</span>
		</div>
	);
};

export default CartIcon;
