// Bootstrap grid system components
// destructuring components/modules produce more cleaner codebase
import { Button, Row, Col, Image, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import '../css/Banner.css';

export default function Banner({ data }) {
	const { title, content, destination, label } = data;
	console.log(data);

	return (
		<Container>
			<Row className="mt-3 mb-3">
				<Container className="bannerBackground">
					<Row>
						<Col xs={8} md={8}>
							<h1 className="display-3 pt-4">
								C H A - E U N - W O O
							</h1>
							<h1 className="custom-header pt-5">ZUITT</h1>
							<h1 className="custom-header">COLLECTION</h1>
							<Button
								className="custom-btn"
								variant="light"
								as={Link}
								to="/register"
							>
								Register
							</Button>
						</Col>
						<Col xs={4} md={4}>
							<Image
								className="img-fluid full-width "
								src="./images/bago.jpg"
								alt="Chaeun Woo"
								rounded
							/>
						</Col>
					</Row>
				</Container>
			</Row>
		</Container>
	);
}
