import React, { useCallback } from "react";
import { Modal, Button } from "react-bootstrap";
import Swal from "sweetalert2";

export default function CartModal({
  show,
  handleClose,
  cartItems,
  setCartItems,
  onRemoveItem,
}) {
  const handleRemoveItem = (productId) => {
    onRemoveItem(productId);
  };
  const handleCheckout = () => {
    const storedUser = localStorage.getItem("user");
    const storedToken = localStorage.getItem("token");
    const initialUser = storedUser ? JSON.parse(storedUser) : null;

    const order = {
      user: initialUser._id,
      products: cartItems.map(({ productId, quantity, product, price }) => ({
        product,
        productId,
        price,
        quantity,
        subtotal: price * quantity,
      })),
      totalAmount: cartItems.reduce(
        (total, { quantity, price }) => total + price * quantity,
        0
      ),
    };

    // Log the POST request before sending
    console.log("Checkout Request:", order);

    fetch("http://localhost:4000/orders/create", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${storedToken}`,
      },
      body: JSON.stringify(order),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log("Order created:", data);
        Swal.fire({
          title: "Checkout Successful",
          icon: "success",
          text: "Your order has been placed successfully.",
        });
        setCartItems([]);
      })
      .catch((error) => {
        console.error("Error creating order:", error);
        Swal.fire({
          title: "Checkout Failed",
          icon: "error",
          text: "There was an error while processing your order. Please try again.",
        });
        // Handle the error condition
      });
  };

  const isEmpty = cartItems.length === 0;

  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Cart Items</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {isEmpty ? (
          <p>No products in the cart. Add a product.</p>
        ) : (
          cartItems.map(
            ({ productId, quantity, price, product, description }) => {
              const subtotal = price * quantity; // Calculate subtotal
              return (
                <div key={productId}>
                  <p>Product: {product}</p>
                  <p>Description: {description}</p>
                  <p>Price: {price}</p>
                  <p>Quantity: {quantity}</p>
                  <p>Subtotal: {subtotal}</p> {/* Display subtotal */}
                  <Button
                    variant="danger"
                    onClick={() => handleRemoveItem(productId)}
                  >
                    Remove
                  </Button>
                  <hr />
                </div>
              );
            }
          )
        )}
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
        {
          /* Display checkout button only if cart is not empty */
          !isEmpty && (
            <Button variant="primary" onClick={handleCheckout}>
              Checkout
            </Button>
          )
        }
      </Modal.Footer>
    </Modal>
  );
}
