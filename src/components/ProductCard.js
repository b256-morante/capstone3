import React, { useState, useEffect } from 'react';
import CartIcon from './CartIcon';
import '../css/ProductCard.css';
import ProductCardContent from './ProductCardContent';
import ProductCardModal from './ProductCardModal';
import Swal from 'sweetalert2';

export default function ProductCard({
	product,
	onAddToCart,
	onQuantityChange,
	onRemoveItem,
}) {
	const { _id, price } = product;
	const [quantity, setQuantity] = useState(1);
	const [totalPrice, setTotalPrice] = useState(price);
	const [modalShow, setModalShow] = useState(false);

	useEffect(() => {
		setTotalPrice(price * quantity);
	}, [price, quantity]);

	const handleAddToCart = () => {
		onAddToCart(_id, quantity);

		// Show SweetAlert notification
		Swal.fire({
			icon: 'success',
			title: 'Product added to cart',
			text: `Product: ${product.product}\nQuantity: ${quantity}`,
			confirmButtonText: 'OK',
		});
	};

	const handleQuantityIncrement = () => {
		setQuantity((prevQuantity) => prevQuantity + 1);
	};

	const handleQuantityDecrement = () => {
		if (quantity === 1) {
			return;
		}
		setQuantity((prevQuantity) => prevQuantity - 1);
	};

	const handleQuantityInputChange = (event) => {
		const newQuantity = parseInt(event.target.value);
		setQuantity(newQuantity);
		onQuantityChange(_id, newQuantity);
	};

	const handleModalOpen = () => {
		setModalShow(true);
	};

	const handleModalClose = () => {
		setModalShow(false);
	};

	const handleRemoveItem = () => {
		onRemoveItem(_id);
	};

	return (
		<div className="product-card-page">
			<div className="product-cards">
				<ProductCardContent
					product={product}
					quantity={quantity}
					totalPrice={totalPrice}
					handleAddToCart={handleAddToCart}
					handleQuantityDecrement={handleQuantityDecrement}
					handleQuantityInputChange={handleQuantityInputChange}
					handleQuantityIncrement={handleQuantityIncrement}
					handleModalOpen={handleModalOpen}
					setModalShow={setModalShow}
					handleRemoveItem={handleRemoveItem}
				/>
			</div>

			<ProductCardModal
				show={modalShow}
				handleClose={handleModalClose}
				product={product}
				quantity={quantity}
			/>
		</div>
	);
}
