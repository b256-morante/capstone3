import React from 'react';
import { Card, Button } from 'react-bootstrap';
import '../css/ProductCard.css';
import { useNavigate } from 'react-router-dom';

export default function ProductCardContent({
  product,
  quantity,
  totalPrice,
  handleAddToCart,
  handleQuantityDecrement,
  handleQuantityInputChange,
  handleQuantityIncrement,
  setModalShow,
}) {
  const { _id, description } = product;

  const handleClickAddToCart = () => {
    if (!isLoggedIn) {
      navigate('/login'); // Redirect to login page if not logged in
      return;
    }

    handleAddToCart(_id, quantity);
    setModalShow(true);
  };

  const storedUser = localStorage.getItem('user');
  const initialUser = storedUser ? JSON.parse(storedUser) : null;
  const isLoggedIn = initialUser !== null;
  const isAdmin = isLoggedIn && initialUser.isAdmin;
  const navigate = useNavigate();

  return (
    <>
      <Card className="w-100 mb-3">
        <Card.Body>
          <Card.Title>{product.product}</Card.Title>
          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{description}</Card.Text>
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>{totalPrice}</Card.Text>
          {isAdmin && (
            <div className="admin-message">You are an admin user.</div>
          )}
          {!isAdmin && (
            <div className="product-actions">
              <Button variant="light" onClick={handleClickAddToCart}>
                Add to Cart
              </Button>
              <div className="quantity-slider">
                <button
                  className="slider-button"
                  onClick={handleQuantityDecrement}
                >
                  -
                </button>
                <input
                  type="number"
                  value={quantity}
                  onChange={handleQuantityInputChange}
                  className="quantity-input"
                />
                <button
                  className="slider-button"
                  onClick={handleQuantityIncrement}
                >
                  +
                </button>
              </div>
            </div>
          )}
        </Card.Body>
      </Card>
    </>
  );
}
