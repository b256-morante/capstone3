import React from 'react';
import { Modal, Button } from 'react-bootstrap';

export default function ProductCardModal({
  show,
  handleClose,
  product,
  quantity,
}) {
  const { product: productName, description, price } = product;

  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>{productName}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p>Description: {description}</p>
        <p>Price: {price}</p>
        <p>Quantity: {quantity}</p>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
