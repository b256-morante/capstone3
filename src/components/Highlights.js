// Bootstrap grid system components
import { Row, Col, Card, Button, Image } from 'react-bootstrap';
import { Container } from 'react-bootstrap';
import '../css/Highlights.css';
export default function Highlights() {
	return (
		<Container>
			<div className="container pb-5">
				<div className="square square1"></div>
				<div className="square square2"></div>
				<div className="square square3"></div>
			</div>
			<h1 class="display-2" className="pt-5 text-center">
				CHA EUN WOO'S PICK
			</h1>
			<Row className="mt-3 mb-3">
				<Col className="p-5 text-center" lg={6} md={12}>
					<Image
						src="./images/cosmo.jpg"
						rounded
						class="image"
						className="img-fluid"
					/>
				</Col>
				<Col
					className="p-5 text-align-justify justify-content"
					lg={6}
					md={12}
				>
					<p class="lead">
						One of things Eun Woo talked about was his current go-to
						perfume, Tom Ford's Soleil Blanc. "I place it in front
						of the door and always put it on before leaving," says
						the actor. Soleil Blanc is described as a warm and spicy
						fragrance with notes of sea coconut (or coco de mer),
						ylang-ylang, and cardamom—a perfect match for someone
						like Eun Woo who prefers a fresh, fruity scent with a
						kick. Another fragrance Eun Woo was spotted wearing
						before is Atelier Cologne's Clementine California, a
						fresh citrus cologne with a vetiver base.The True Beauty
						star's perfume application method was simple enough: He
						spritzes it twice on his wrist and then rubs it on to
						his other wrist and the sides of his neck.
					</p>
				</Col>
			</Row>
			<Container>
				<h1 class="display-2" className="pt-5 text-center">
					MEET OUR AMBASSADORS
				</h1>
				<Row className="mt-3 mb-3">
					<Col className="p-5 " lg={4} md={4} sm={4}>
						<Image
							src="./images/yoon.jpg"
							rounded
							class="image"
							className="img-fluid"
						/>
						<h1 class="display-2" className="pt-5 text-center">
							SEUNGYOON
						</h1>
					</Col>
					<Col className="p-5 " lg={4} md={4} sm={4}>
						<Image
							src="./images/sehun.jpg"
							rounded
							class="image"
							className="img-fluid"
						/>
						<h1 class="display-2" className="pt-5 text-center">
							SEHUN
						</h1>
					</Col>
					<Col className="p-5 " lg={4} md={4} sm={4}>
						<Image
							src="./images/jin.jpg"
							rounded
							class="image"
							className="img-fluid"
						/>
						<h1 class="display-2" className="pt-5 text-center">
							SEOKJIN
						</h1>
					</Col>
				</Row>
			</Container>
		</Container>
	);
}
