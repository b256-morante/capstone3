import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../userContext';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function AddProduct() {
	const [product, setProduct] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [isActive, setIsActive] = useState(false);
	const { user, setUser } = useContext(UserContext);
	const token = localStorage.getItem('token');

	console.log(token);

	useEffect(() => {
		if (product !== '' && description !== '' && price !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	});

	function createProduct(e) {
		e.preventDefault();

		// clear input fields
		setProduct('');
		setDescription('');
		setPrice('');

		fetch('http://localhost:4000/product/create', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`,
			},
			body: JSON.stringify({
				product: product,
				description: description,
				price: price,
			}),
		});

		Swal.fire({
			title: 'Product Successfully Added',
			icon: 'success',
			text: 'Product is now available',
		});
	}

	return (
		<Form onSubmit={(e) => createProduct(e)}>
			<Form.Group className="mb-3" controlId="formBasicProduct">
				<Form.Label>Product</Form.Label>
				<Form.Control
					type="product"
					placeholder="Enter Product Name"
					value={product}
					onChange={(e) => setProduct(e.target.value)}
				/>
			</Form.Group>
			<Form.Group className="mb-3" controlId="formBasicDescription">
				<Form.Label>Description</Form.Label>
				<Form.Control
					type="description"
					placeholder="Enter Description"
					value={description}
					onChange={(e) => setDescription(e.target.value)}
				/>
			</Form.Group>
			<Form.Group className="mb-3" controlId="formBasicPrice">
				<Form.Label>Price</Form.Label>
				<Form.Control
					type="price"
					placeholder="Enter price"
					value={price}
					onChange={(e) => setPrice(e.target.value)}
				/>
			</Form.Group>

			{/*Ternary Operator*/}
			{/*
			if (isActive === true) {
				<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
			} else {
				<Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
			}
	  	  */}
			{isActive ? (
				<Button variant="primary" type="submit" id="submitBtn">
					Submit
				</Button>
			) : (
				<Button variant="danger" type="submit" id="submitBtn" disabled>
					Submit
				</Button>
			)}
		</Form>
	);
}
