import React, { useEffect, useState } from 'react';
import { Container, Table, Button, Row, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';
import ProductUpdateModal from './ProductUpdateModal';
import '../css/DashBoard.css';
import { Navigate, Link } from 'react-router-dom';

function AdminDashboard() {
  const [products, setProducts] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [selectedProductId, setSelectedProductId] = useState(null);
  const storedToken = localStorage.getItem('token');
  useEffect(() => {
    // Fetch all products
    fetch('http://localhost:4000/product/all', {
      headers: {
        Authorization: `Bearer ${storedToken}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        console.log('All Products:', data);
        setProducts(data);
      })
      .catch((error) => {
        console.error('Error fetching user orders:', error);
      });
  }, [storedToken]);

  const [redirectToCreate, setRedirectToCreate] = useState(false);

  const handleCreateButtonClick = () => {
    setRedirectToCreate(true);
  };

  if (redirectToCreate) {
    return <Navigate to="/product/create" />;
  }
  // ...
  // ...

  const handleArchiveProduct = (productId) => {
    // Find the product to determine if it is currently active or archived
    const productToArchive = products.find(
      (product) => product._id === productId
    );

    // Determine the desired action based on the current state of the product
    const action = productToArchive.isActive ? 'archive' : 'reactivate';

    // Fetch request to perform the action on the product
    fetch(`http://localhost:4000/product/${action}/${productId}`, {
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${storedToken}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        // Handle the response data after archiving or reactivating the product
        console.log('Product updated:', data);
        // Show a success message

        Swal.fire({
          icon: 'success',
          title:
            action === 'archive' ? 'Product Archived' : 'Product Reactivated',
          text:
            action === 'archive'
              ? 'The product has been archived successfully.'
              : 'The product has been reactivated successfully.',
        });
        // Update the product list after archiving or reactivating
        setProducts((prevProducts) =>
          prevProducts.map((product) =>
            product._id === productId
              ? { ...product, isActive: !product.isActive }
              : product
          )
        );
      })
      .catch((error) => {
        // Handle any errors that occurred during archiving or reactivating
        console.error(`Error ${action} product:`, error);
        // Show an error message
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: `Failed to ${action} the product.`,
        });
      });
  };

  const handleReactivateProduct = (productId) => {
    // Fetch request to reactivate the product
    fetch(`http://localhost:4000/product/reactivate/${productId}`, {
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${storedToken}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        // Handle the response data after reactivating the product
        console.log('Product reactivated:', data);
        // Show a success message
        Swal.fire({
          icon: 'success',
          title: 'Product Reactivated',
          text: 'The product has been reactivated successfully.',
        });
        // Update the product list after reactivating
        setProducts((prevProducts) =>
          prevProducts.map((product) =>
            product._id === productId ? { ...product, isActive: true } : product
          )
        );
      })
      .catch((error) => {
        // Handle any errors that occurred during reactivation
        console.error('Error reactivating product:', error);
        // Show an error message
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'Failed to reactivate the product.',
        });
      });
  };

  const handleUpdateProduct = (productId, updatedData) => {
    // Perform the update request with the provided product ID and updated data
    fetch(`http://localhost:4000/product/update/${productId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${storedToken}`,
      },
      body: JSON.stringify(updatedData),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log('Product updated:', data);
        // Update the products state with the updated product data
        setProducts((prevProducts) =>
          prevProducts.map((product) => {
            if (product._id === productId) {
              return {
                ...product,
                ...updatedData,
              };
            }
            return product;
          })
        );
        // Show a success message
        Swal.fire({
          icon: 'success',
          title: 'Product Updated',
          text: 'The product has been updated successfully.',
        });
      })
      .catch((error) => {
        console.error('Error updating product:', error);
        // Show an error message
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'Failed to update the product.',
        });
      });

    // Close the modal after the update
    setShowModal(false);
    setSelectedProductId(null);
  };

  return (
    <Container>
      <h3 className="mt-3 mb-3 display-2" w-50>
        All Products
      </h3>

      <Button
        variant="secondary"
        className="w-75 mb-3"
        onClick={handleCreateButtonClick}
      >
        Create Product
      </Button>

      {products.length > 0 ? (
        <div className="table-container">
          <Table className="product-table">
            <thead>
              <tr>
                <th>Product</th>
                <th>Description</th>
                <th>Price</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {products.map((product) => (
                <tr key={product._id}>
                  <td>{product.product}</td>
                  <td>{product.description}</td>
                  <td>{product.price}</td>
                  <td>
                    <Button
                      variant="secondary"
                      className="m-2 update-button w-75"
                      onClick={() => {
                        setSelectedProductId(product._id);
                        setShowModal(true);
                      }}
                    >
                      Update
                    </Button>
                    {product.isActive ? (
                      <Button
                        variant="light"
                        className="m-2 archive-button w-75"
                        onClick={() => handleArchiveProduct(product._id)}
                      >
                        Archive
                      </Button>
                    ) : (
                      <Button
                        variant="dark"
                        className="m-2 inactive-button w-75"
                        onClick={() => handleReactivateProduct(product._id)}
                      >
                        Reactivate
                      </Button>
                    )}
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      ) : (
        <p className="no-products">No products found.</p>
      )}
      {showModal && (
        <ProductUpdateModal
          productId={selectedProductId}
          handleCloseModal={() => setShowModal(false)}
          handleUpdateProduct={handleUpdateProduct}
        />
      )}
    </Container>
  );
}

export default AdminDashboard;
