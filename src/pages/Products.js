import React, { useState, useEffect } from 'react';
import ProductCard from '../components/ProductCard';
import CartIcon from '../components/CartIcon';
import CartModal from '../components/CartModal';
import Swal from 'sweetalert2';
import { Row, Col } from 'react-bootstrap';
import '../css/Products.css';
export default function Products() {
  const [products, setProducts] = useState([]);
  const [cartCount, setCartCount] = useState(0);
  const [cartItems, setCartItems] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);

  const handleAddToCart = async (productId, quantity) => {
    // Create a new cart item object with productId and quantity
    const response = await fetch(`http://localhost:4000/product/${productId}`);
    const product = await response.json();

    const newCartItem = {
      product: product.product,
      description: product.description,
      price: product.price,
      productId: productId,
      quantity: quantity,
    };

    // Check if the cart item already exists in the cartItems array
    const existingCartItemIndex = cartItems.findIndex(
      (item) => item.productId === productId
    );
    if (existingCartItemIndex !== -1) {
      // If the cart item already exists, update the quantity
      const updatedCartItems = [...cartItems];
      updatedCartItems[existingCartItemIndex].quantity += quantity;
      setCartItems(updatedCartItems);
    } else {
      // If the cart item doesn't exist, add it to the cartItems array
      setCartItems((prevItems) => [...prevItems, newCartItem]);
    }

    // Update the cart count
    setCartCount((prevCount) => prevCount + quantity);

    // Show SweetAlert notification
    Swal.fire({
      icon: 'success',
      title: 'Product added to cart',
      text: `Product ID: ${productId}\nQuantity: ${quantity}`,
      confirmButtonText: 'OK',
    });
  };

  const handleQuantityInputChange = (productId, quantity) => {
    console.log('Product:', productId, 'Quantity:', quantity);
  };

  const handleModalOpen = () => {
    setModalVisible(true);
  };

  const handleModalClose = () => {
    setModalVisible(false);
  };

  const handleRemoveItem = (itemId) => {
    console.log('Remove item:', itemId);

    setCartItems((prevItems) =>
      prevItems.filter((item) => item.productId !== itemId)
    );

    setCartCount(
      (prevCount) =>
        prevCount -
        cartItems.find((item) => item.productId === itemId)?.quantity
    );

    console.log('Updated cartItems:', cartItems);
  };

  useEffect(() => {
    // Fetch product data from the server
    fetch('http://localhost:4000/product/active')
      .then((res) => res.json())
      .then((data) => {
        setProducts(data);
      });
  }, []);

  return (
    <>
      <h1 className="display-3 pt-4 pb-4">PRODUCTS</h1>

      <Row>
        {products.map((product) => (
          <Col key={product._id} sm={12} md={4}>
            <ProductCard
              key={product._id}
              product={product}
              onAddToCart={handleAddToCart}
              onQuantityChange={handleQuantityInputChange}
              handleModalOpen={handleModalOpen}
              onRemoveItem={handleRemoveItem} // Pass the remove function as a prop
            />
          </Col>
        ))}
      </Row>
      <CartIcon onClick={handleModalOpen} />

      <CartModal
        show={modalVisible}
        handleClose={handleModalClose}
        cartItems={cartItems}
        setCartItems={setCartItems}
        products={products}
        onRemoveItem={handleRemoveItem}
      />
    </>
  );
}
