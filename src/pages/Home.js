import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Image from 'react-bootstrap/Image';

export default function Home() {
	const data = {
		title: 'Budolan na',
		content: 'Okay lang maging mahirap, basta may picture ng Koreano',
		destination: '/register',
		label: 'Register Now!',
	};

	return (
		<>
			<Banner data={data} />
			<Highlights />
		</>
	);
}
