import React, { useState, useEffect } from 'react';
import { Modal, Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import '../css/ProductUpdateModal.css';

function ProductUpdateModal({
  productId,
  storedToken,
  handleCloseModal,
  handleUpdateProduct,
}) {
  const [formData, setFormData] = useState({
    product: '',
    description: '',
    price: '',
    isActive: '',
  });

  useEffect(() => {
    // Fetch the product data using the provided product ID
    fetch(`http://localhost:4000/product/${productId}`, {
      headers: {
        Authorization: `Bearer ${storedToken}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        // Update the form data with the fetched product data
        setFormData(data);
      })
      .catch((error) => {
        console.error('Error fetching product:', error);
      });
  }, [productId, storedToken]);

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: value,
    }));
  };

  const handleFormSubmit = (event) => {
    event.preventDefault();
    handleUpdateProduct(productId, formData);
  };

  return (
    <Modal show={true} onHide={handleCloseModal}>
      <Modal.Header closeButton>
        <Modal.Title>Update Product</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={handleFormSubmit}>
          <Form.Group controlId="product">
            <Form.Label>Product</Form.Label>
            <Form.Control
              type="text"
              name="product"
              value={formData.product}
              onChange={handleInputChange}
              placeholder="Enter product name"
              required
            />
          </Form.Group>
          <Form.Group controlId="description">
            <Form.Label>Description</Form.Label>
            <Form.Control
              as="textarea"
              rows={3}
              name="description"
              value={formData.description}
              onChange={handleInputChange}
              placeholder="Enter product description"
              required
            />
          </Form.Group>
          <Form.Group controlId="price" className="mb-3">
            <Form.Label>Price</Form.Label>
            <Form.Control
              type="number"
              name="price"
              value={formData.price}
              onChange={handleInputChange}
              placeholder="Enter product price"
              required
            />
          </Form.Group>
          <Form.Group controlId="isActive" className="mb-3">
            <Form.Label>isActive</Form.Label>
            <Form.Control
              type="boolean"
              name="isActive"
              value={formData.isActive}
              onChange={handleInputChange}
              placeholder="Enter isActive"
              disabled
            />
          </Form.Group>
          <Button variant="light" type="submit">
            Update
          </Button>
        </Form>
      </Modal.Body>
    </Modal>
  );
}

export default ProductUpdateModal;
