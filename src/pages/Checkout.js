import { useState } from "react";

export default function createOrder() {
  const [order, setOrder] = useState(null);

  fetch("http://localhost:4000/orders/create", {
    userId: userId,
    products: products,
    totalAmount: totalAmount,
  })
    .then((response) => {
      setOrder(response.data);
      console.log("Order created successfully:", response.data);
    })
    .catch((error) => {
      console.log("Error creating order:", error);
    });

  return;
}
