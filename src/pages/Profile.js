import React, { useEffect, useState } from 'react';

const Profile = () => {
  const [orders, setOrders] = useState([]);
  const [products, setProducts] = useState([]);

  const storedToken = localStorage.getItem('token');
  console.log(`storedToken: ${storedToken}`);

  useEffect(() => {
    // Fetch user orders
    fetch('http://localhost:4000/orders/myOrder', {
      headers: {
        Authorization: `Bearer ${storedToken}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        console.log('User Orders:', data);
        setOrders(data);
        fetchProducts(data);
      })
      .catch((error) => {
        console.error('Error fetching user orders:', error);
      });
    console.log(`orders: ${orders}`);
  }, [storedToken]);

  const fetchProducts = async (orders) => {
    const productIds = orders.reduce((ids, order) => {
      order.products.forEach((product) => ids.add(product.productId));
      return ids;
    }, new Set());

    try {
      const response = await fetch('http://localhost:4000/product/all', {
        headers: {
          Authorization: `Bearer ${storedToken}`,
        },
      });

      if (!response.ok) {
        throw new Error('Failed to fetch products');
      }

      const products = await response.json();

      // Filter the products based on the retrieved product IDs
      const filteredProducts = products.filter((product) =>
        productIds.has(product._id)
      );

      setProducts(filteredProducts);
    } catch (error) {
      console.error('Error fetching products:', error);
    }
  };

  const getProductDetails = (productId) => {
    const product = products.find((product) => product._id === productId);
    return product ? product : null;
  };

  return (
    <div>
      <h3 className="mb-3">My Orders</h3>
      <h4 className="mb-3">Ordered Products</h4>
      {orders.length > 0 ? (
        orders.map((order) => (
          <div key={order._id}>
            <p>Order ID: {order._id}</p>
            <p>Total Amount: {order.totalAmount}</p>
            {order.products.map((product) => {
              const productDetails = getProductDetails(product.productId);
              return productDetails ? (
                <div key={product.productId}>
                  <p>Product ID: {product.productId}</p>
                  <p>Quantity: {product.quantity}</p>
                  <p>Product: {productDetails.product}</p>
                  <p>Price: {productDetails.price}</p>
                  <p>Description: {productDetails.description}</p>
                  {/* Display other product details */}
                </div>
              ) : null;
            })}
            <hr />
          </div>
        ))
      ) : (
        <p>No orders found.</p>
      )}
    </div>
  );
};

export default Profile;
