import { useState, useEffect, useContext } from 'react';
import { Form, Button, Modal } from 'react-bootstrap';
import UserContext from '../userContext';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import '../css/Register.css';

export default function Register() {
  const { user, setUser } = useContext(UserContext);
  const navigate = useNavigate();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [isActive, setIsActive] = useState(false);
  const [isRegistered, setIsRegistered] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const [showModal, setShowModal] = useState(true);

  useEffect(() => {
    if (email !== '' && password !== '' && confirmPassword !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password, confirmPassword]);

  function registerUser(e) {
    e.preventDefault();

    if (password !== confirmPassword) {
      setErrorMessage('Passwords do not match.');
      return;
    }

    if (password.length < 6) {
      setErrorMessage('Password should be at least 6 characters long.');
      return;
    }

    // Send registration request to the backend API
    fetch('http://localhost:4000/users/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
        password: password,
        confirmPassword: confirmPassword,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.error) {
          setErrorMessage(data.error);
        } else {
          setIsRegistered(true);
          Swal.fire({
            title: 'Registration Successful',
            icon: 'success',
            text: 'Welcome to Annyeong Chingu Store',
          }).then(() => {
            navigate('/login');
          });
        }
      })
      .catch((error) => {
        setErrorMessage('An error occurred. Please try again later.');
      });
  }

  if (isRegistered) {
    return null; // No need for Navigate component, as we are using the navigate function
  }

  const handleCloseModal = () => {
    setShowModal(false);
    navigate('/home');
  };

  return (
    <Modal
      className="registerz"
      show={showModal}
      onHide={handleCloseModal}
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>Register</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={registerUser}>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="formBasicConfirmPassword">
            <Form.Label>Confirm Password</Form.Label>
            <Form.Control
              className="mb-3"
              type="password"
              placeholder="Confirm Password"
              value={confirmPassword}
              onChange={(e) => setConfirmPassword(e.target.value)}
            />
          </Form.Group>
          {errorMessage && <p className="text-danger">{errorMessage}</p>}
          <Button
            className="w-100"
            variant="secondary"
            type="submit"
            disabled={!isActive}
          >
            Register
          </Button>
        </Form>
      </Modal.Body>
    </Modal>
  );
}
