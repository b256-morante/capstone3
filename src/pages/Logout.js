import { useContext, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import UserContext from '../userContext';

export default function Logout() {
	const { unsetUser } = useContext(UserContext);
	const navigate = useNavigate();

	useEffect(() => {
		const logout = () => {
			unsetUser();
			setTimeout(() => navigate('/home'), 2000); // Redirect to home after 2 seconds
		};

		logout();
	}, [unsetUser, navigate]);

	return <p>You have been logged out. Redirecting to the home page...</p>;
}
