import './App.css';
import Login from './pages/Login';
import Register from './pages/Register';
import AppNavBar from './components/AppNavBar';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { UserProvider } from './userContext';
import Home from './pages/Home';
import AddProduct from './pages/AddProduct';
import Products from './pages/Products';
import Logout from './pages/Logout';
import Profile from './pages/Profile';
import AdminDashBoard from './pages/AdminDashboard';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  // Function for clearing the localStorage when a user logout
  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  });
  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavBar />
        <Container>
          <Routes>
            <Route path="/login" element={<Login />} />
            <Route path="/" element={<Home />} />
            <Route path="/register" element={<Register />} />
            <Route path="/home" element={<Home />} />
            <Route path="/create" element={<AddProduct />} />
            <Route path="/product/active" element={<Products />} />
            <Route path="/profile" element={<Profile />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/profile/admin" element={<AdminDashBoard />} />
            <Route path="/product/create" element={<AddProduct />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
